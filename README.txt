       Twitt 
     ---------
Twitt is a software project. It simulates twitter-like message spread within social network.
Currently four main type of activitieas are supported:
- Posting messages from user.
- Reading messages, filtered by author.
- Following user.
- Reading user's wall.


System Requirements
-------------------
JDK:
   JSE 1.7.0_07 or above
   Download link : http://www.oracle.com/technetwork/java/javase/downloads/index.html
Maven:
   3.0.4 or above
   Download link : http://maven.apache.org/download.cgi


Installation and running
------------------------
1. Enter the folder, where pom.xml localted.This is the folder on the top of downloaded project.
2. Make sure JAVA_HOME is set correclty and locate to your JDK.
3. Make sure maven is installed correctly. To check run "mvn --version".
4. Run "mvn package" command. The message 'SUCCED' should appear as result of the run.
5. Enter the 'target' forlder.
6. To run the program enter 'java -jar twitt-1.0.1.jar' command.
7. Have a fun :)

User's guide
------------
1) Sending message to the twitt.

command format:
USERNAME -> MESSAGE

USERNAME should consists of upper and lowercase letters and/or digits. No spaces and special characters allowed.
MESSAGE may contain any number of any characters

Sends the MESSAGE from the name of USERNAME. No message expected to be shown as a result.

2) Reading messages by author

command format:
USERNAME

Shows all messages posted by USERNAME

3) Following user.

command format:
USERNAME1 follows USERNAME2

Allows to see all messeges, posted by USERNAME2, on USERNAME1's wall. No message expecte to be shown.

4) Reading user's wall.

command format:
USERNAME wall

Show's all the messages, posted by user. Also shows all the messages from other users followed by USERNAME.

5) Exit

To exit program simple enter empty command.
