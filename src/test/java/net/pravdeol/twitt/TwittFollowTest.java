package net.pravdeol.twitt;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

public class TwittFollowTest extends BaseTwittTestClass{

    @Test
    public void testFollowUnexistingUser() {
        final List<String> response = enter("Alice follows Bob", 0L);
        assertTrue(response.isEmpty());
    }
    
    @Test
    public void testFollowSelf() {
        final List<String> response = enter("Alice follows Alice", 0L);
        assertTrue(response.isEmpty());
    }
    
    @Test
    public void testFollow() {
        enter("Bob -> Hi all", 0L);
        enter("Alice -> Hi Bob", 1_000L);
        
        final List<String> response = enter("Bob follows Alice", 5_000L);
        assertTrue(response.isEmpty());
    }
    
}
