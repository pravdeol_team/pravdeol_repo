package net.pravdeol.twitt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

public class TwittReadMessageTest extends BaseTwittTestClass{
    
    @Test
    public void testReadUnexistingUser() {
        final List<String> responde = enter("Alice", 0L);
        assertNotNull(responde);
        assertTrue(responde.isEmpty());
    }
    
    @Test
    public void testReadMessage() {
        enter("Alice -> my first message", 0L);                  // post message
        final List<String> responde = enter("Alice", 5_000L);        // read Alice's timeline
        assertEquals(1, responde.size());
        assertEquals("my first message (5 seconds ago)", responde.get(0));
    }
    
    @Test
    public void testReadMessageWithExtraSpaces() {
        enter("Alice   ->   my first message   ", 0L);                  // post message
        final List<String> responde = enter("Alice  ", 5_000L);        // read Alice's timeline
        assertEquals(1, responde.size());
        assertEquals("my first message (5 seconds ago)", responde.get(0));
    }
    
    @Test
    public void testReadMessageWithDigits() {
        enter("Alice -> my 1-st message", 0L);                  // post message with digits and special symbols
        final List<String> responde = enter("Alice", 5_000L);        // read Alice's timeline
        assertEquals(1, responde.size());
        assertEquals("my 1-st message (5 seconds ago)", responde.get(0));
    }
    
    @Test
    public void testReadMessage_UsernameWithDigits() {
        enter("abc1d -> some message", 0L);                  // post message
        final List<String> responde = enter("abc1d", 55_000L);        // read abc1d's timeline
        assertEquals(1, responde.size());
        assertEquals("some message (55 seconds ago)", responde.get(0));
    }

    @Test
    public void testReadMessageFromAnotherUser() {
        enter("abc1 -> some message", 0L);                  // post message
        final List<String> responde = enter("abc", 1_000L);        // read abc's timeline, which should be empty
        assertTrue(responde.isEmpty());
    }
    
    @Test
    public void testReadTwoMessages() {
        enter("Alice -> Morning All!!!", 1_000L);
        enter("Alice -> This is my second post", 5_000L);
        
        final List<String> response = enter("Alice", 10_000L);
        
        assertEquals(2, response.size());
        assertEquals("This is my second post (5 seconds ago)", response.get(0));
        assertEquals("Morning All!!! (9 seconds ago)", response.get(1));
    }
}
