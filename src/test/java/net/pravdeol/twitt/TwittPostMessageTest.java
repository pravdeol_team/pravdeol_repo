package net.pravdeol.twitt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

public class TwittPostMessageTest extends BaseTwittTestClass{

    @Test
    public void testPostFirstMessage() {
        final List<String> responde = enter("Alice -> hello", 0L);
        assertNotNull(responde);
        assertTrue(responde.isEmpty());
    }
    
    @Test
    public void testBadFormattedCommand() {
        final List<String> responde = enter("Alice - > hello", 0L);
        assertNotNull(responde);
        assertEquals(1, responde.size());
        assertEquals("Bad formatted message.", responde.get(0)); // error message is not specified
    }
}
