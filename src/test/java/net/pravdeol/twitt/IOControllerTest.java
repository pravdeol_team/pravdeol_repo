package net.pravdeol.twitt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class IOControllerTest {

    @Mock
    private Clock clock;
    @Mock
    private BufferedReader input;
    @Mock
    private PrintStream print;
    private IOController main;
    
    @Before
    public void beforeTest() {
        main = new IOController(input, print, clock);
        Mockito.when(clock.currentTime()).thenReturn(0L);
    }

    @Test
    public void testPromptExit() throws IOException {
        //exits immediately
        Mockito.when(input.readLine()).thenReturn("");
        
        main.readAndProcess();

        final InOrder inOrder = Mockito.inOrder(input, print);
        inOrder.verify(print).print(">");
        inOrder.verify(input).readLine();
        inOrder.verifyNoMoreInteractions();
    }
    
    @Test
    public void testExitWithNullInput() throws IOException {
        //exits immediately
        Mockito.when(input.readLine()).thenReturn(null);
        
        main.readAndProcess();

        final InOrder inOrder = Mockito.inOrder(input, print);
        inOrder.verify(print).print(">");
        inOrder.verify(input).readLine();
        inOrder.verifyNoMoreInteractions();
    }
    
    @Test
    public void testInputOutputInteractions() throws IOException {
        Mockito.when(input.readLine())
            .thenReturn("Alice -> Hello.")
            .thenReturn("Alice")
            .thenReturn("");
        
        main.readAndProcess();

        final InOrder inOrder = Mockito.inOrder(input, print);
        inOrder.verify(print).print(">");
        inOrder.verify(input).readLine(); // enter message
        inOrder.verify(print).print(">");
        inOrder.verify(input).readLine(); // read Alice's message
        inOrder.verify(print).println();
        inOrder.verify(print).println("Hello. (1 second ago)");
        inOrder.verify(print).println();
        inOrder.verify(print).print(">"); // exit
        inOrder.verify(input).readLine();
        inOrder.verifyNoMoreInteractions();
    }
    
}
