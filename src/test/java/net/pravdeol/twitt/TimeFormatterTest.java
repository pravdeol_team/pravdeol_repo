package net.pravdeol.twitt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeFormatterTest {

    private final TimeFormatter timeFormatter = new TimeFormatter();
    
    @Test
    public void testZero() {
//        time counting starts with 1 second
        assertEquals("1 second", timeFormatter.millisToReadableRoundedTime(0));
    }
    
    @Test
    public void testOneSecondRoundingUp() {
        assertEquals("1 second", timeFormatter.millisToReadableRoundedTime(1));
    }
    
    @Test
    public void testOneSecondRoundingUp2() {
        assertEquals("1 second", timeFormatter.millisToReadableRoundedTime(500));
    }

    @Test
    public void testOneSecond() {
        assertEquals("1 second", timeFormatter.millisToReadableRoundedTime(1000));
    }

    @Test
    public void testOneSecondRoundingDown() {
        assertEquals("1 second", timeFormatter.millisToReadableRoundedTime(1499));
    }

    @Test
    public void testTwoSecondsRoundingUp() {
        assertEquals("2 seconds", timeFormatter.millisToReadableRoundedTime(1500));
    }

    @Test
    public void testTwoSeconds() {
        assertEquals("2 seconds", timeFormatter.millisToReadableRoundedTime(2000));
    }

    @Test
    public void test59seconds() {
        assertEquals("59 seconds", timeFormatter.millisToReadableRoundedTime(59_499));
    }

    @Test
    public void testOneMinuteRoundingUp() {
        assertEquals("1 minute", timeFormatter.millisToReadableRoundedTime(59_500));
    }

    @Test
    public void testOneMinuteRoundingDown() {
        assertEquals("1 minute", timeFormatter.millisToReadableRoundedTime(60_499));
    }

    @Test
    public void testSevenMinutes() {
        assertEquals("7 minutes", timeFormatter.millisToReadableRoundedTime(420_000));
    }

    @Test
    public void testOneHourRoundingUp() {
        assertEquals("1 hour", timeFormatter.millisToReadableRoundedTime(3_599_500));
    }

    @Test
    public void testOneHourRoundingDown() {
        assertEquals("1 hour", timeFormatter.millisToReadableRoundedTime(3_600_499));
    }

    @Test
    public void testSevenHoursRoundingDown() {
        assertEquals("7 hours", timeFormatter.millisToReadableRoundedTime(7 * 3_600_000));
    }
}
