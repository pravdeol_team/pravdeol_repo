package net.pravdeol.twitt.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.Test;

public class TwittCommandTest {

    @Test
    public void testErrorCommand() {
        assertFalse(TwittCommand.ERROR_COMMAND.accept());
        final List<String> message = TwittCommand.ERROR_COMMAND.execute();
        assertEquals(1, message.size());
        assertEquals("Bad formatted message.", message.get(0));
    }
    
}
