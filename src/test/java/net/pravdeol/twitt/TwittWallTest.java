package net.pravdeol.twitt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

public class TwittWallTest extends BaseTwittTestClass{

    @Test
    public void testViewOwnWall_Empty() {
        final List<String> response = enter("Alice wall", 0L);
        assertTrue(response.isEmpty());
    }
    
    @Test
    public void testViewOwnWall_OwnMessageOnly() {
        enter("Alice -> Hello World.", 0L);
        
        final List<String> response = enter("Alice wall", 1L);
        
        assertEquals(1, response.size());
        assertEquals("Alice - Hello World. (1 second ago)", response.get(0));
    }
    
    @Test
    public void testViewOwnWall_OtherMessageOnly() {
        enter("Alice -> Hello World!!!", 0L);
        enter("Bob follows Alice", 1L);
        
        final List<String> response = enter("Bob wall", 10_000L);
        
        assertEquals(1, response.size());
        assertEquals("Alice - Hello World!!! (10 seconds ago)", response.get(0));
    }
    
    @Test
    public void testWall() {
        enter("Alice -> I love the weather today", 0L);
        enter("Bob -> Oh, we lost!", 180_000L);
        enter("Bob -> at least it's sunny", 240_000L);
        enter("Charlie -> I'm in New York today! Anyone wants to have a coffee?", 298_000L);
        enter("Charlie follows Alice", 300_000L); // 5 min since first message

        final List<String> response = enter("Charlie wall", 300_001L);
        
        assertEquals(2, response.size());
        assertEquals("Charlie - I'm in New York today! Anyone wants to have a coffee? (2 seconds ago)", response.get(0));
        assertEquals("Alice - I love the weather today (5 minutes ago)", response.get(1));
    }
    
    @Test
    public void testWall2() {
        enter("Alice -> I love the weather today", 0L);
        enter("Bob -> Oh, we lost!", 180_000L);
        enter("Bob -> at least it's sunny", 240_000L);
        enter("Charlie -> I'm in New York today! Anyone wants to have a coffee?", 298_000L);
        enter("Charlie follows Alice", 300_000L); // 5 min since first message
        enter("Charlie follows Bob", 300_001L);

        final List<String> response = enter("Charlie wall", 300_002L);

        assertEquals(4, response.size());
        assertEquals("Charlie - I'm in New York today! Anyone wants to have a coffee? (2 seconds ago)", response.get(0));
        assertEquals("Bob - at least it's sunny (1 minute ago)", response.get(1));
        assertEquals("Bob - Oh, we lost! (2 minutes ago)", response.get(2));
        assertEquals("Alice - I love the weather today (5 minutes ago)", response.get(3));
    }
    
    @Test
    public void testUnexistingWall() {
        enter("Alice -> I love the weather today", 0L);

        final List<String> response = enter("Charlie wall", 300_002L);

        assertTrue(response.isEmpty());
    }

    @Test
    public void testFollwingSelfWall() {
        enter("Alice -> I love the weather today", 0L);
        enter("Alice follows Alice", 200_000L);

        final List<String> response = enter("Alice wall", 300_002L);

        assertEquals(1, response.size());
        assertEquals("Alice - I love the weather today (5 minutes ago)", response.get(0));
    }
}
