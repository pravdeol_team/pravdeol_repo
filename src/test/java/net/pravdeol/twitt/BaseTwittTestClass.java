package net.pravdeol.twitt;

import java.util.List;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public abstract class BaseTwittTestClass{

    protected Twitt twitt;
    @Mock
    protected Clock clock;
    
    @Before
    public void beforeTest() {
        twitt = new Twitt(clock);
    }

    protected List<String> enter(final String input, final long time) {
        Mockito.when(clock.currentTime()).thenReturn(time);
        return twitt.process(input);
    }

}
