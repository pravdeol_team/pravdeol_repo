package net.pravdeol.twitt.command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.pravdeol.twitt.Clock;
import net.pravdeol.twitt.Message;
import net.pravdeol.twitt.TimeFormatter;

public class ReadUserCommand extends AbstractTwittCommand {

    private static final String COMMAND_REGEXP_STRING = SPACE + "(" + USERNAME + ")" + SPACE;
    private final List<Message> twittLine;
    
    public ReadUserCommand(final Clock clock, final List<Message> twittLine, final String pattern, final String input) {
        super(clock, pattern, input);
        this.twittLine = twittLine;
    }

    protected ReadUserCommand(final Clock clock, final List<Message> twittLine, final String input) {
        this(clock, twittLine, COMMAND_REGEXP_STRING, input);
    }

    @Override
    public List<String> execute() {
        final List<Message> filteredMessages = filterMessages();
        final List<String> formattedMessages = format(filteredMessages);
        Collections.reverse(formattedMessages);
        return formattedMessages;
    }
    
    protected boolean isMessageToShow(final Message message) {
        return isAuthorOfMessage(message);
    }

    protected final boolean isAuthorOfMessage(final Message message) {
        final String author = matcher.group(1);
        return message.getAuthor().equals(author);
    }

    protected String format(final Message message) {
        final String timePassed = timePassed(message);
        return String.format("%s (%s ago)", message.getText(), timePassed);
    }

    protected String timePassed(final Message message) {
        final long timePassedInMillis = clock.currentTime() - message.getCreationTime();
        final String timePassed = new TimeFormatter().millisToReadableRoundedTime(timePassedInMillis);
        return timePassed;
    }

    private List<String> format(final List<Message> filteredMessages) {
        final List<String> formatedStrings = new ArrayList<>();
        for (final Message message : filteredMessages) {
            final String formattedMessage = format(message);
            formatedStrings.add(formattedMessage);
        }
        return formatedStrings;
    }

    private List<Message> filterMessages() {
        final List<Message> filteredMessages = new ArrayList<>();
        for (final Message message: twittLine) {
            if (isMessageToShow(message)) {
                filteredMessages.add(message);
            }
        }
        return filteredMessages;
    }

}
