package net.pravdeol.twitt.command;

import java.util.Collections;
import java.util.List;

public interface TwittCommand {

    TwittCommand ERROR_COMMAND = new TwittCommand() {
        
        @Override
        public List<String> execute() {
            return Collections.singletonList("Bad formatted message.");
        }
        
        @Override
        public boolean accept() {
            return false;
        }
    };
    
    boolean accept();
    List<String> execute();
    
}
