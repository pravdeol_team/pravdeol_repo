package net.pravdeol.twitt.command;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.pravdeol.twitt.Clock;

public class FollowUserCommand extends AbstractTwittCommand {

    private static final String COMMAND_REGEXP_STRING = "\\s*(" + USERNAME + ")\\s* follows \\s*(" + USERNAME + ")\\s*";
    private Map<String, Set<String>> followedByUser;
    
    public FollowUserCommand(final Clock clock, final Map<String, Set<String>> followedByUser, final String input) {
        super(clock, COMMAND_REGEXP_STRING, input);
        this.followedByUser = followedByUser;
    }

    @Override
    public List<String> execute() {
        final String follower = matcher.group(1);
        final String followed = matcher.group(2);
        addFollower(follower, followed);
        return Collections.emptyList();
    }

    private void addFollower(final String follower, final String followed) {
        final Set<String> followedSet = followedByUser.get(follower);
        if (followedSet == null) {
            createFollowedSet(follower, followed);
        } else {
            followedSet.add(followed);
        }
    }

    private void createFollowedSet(final String follower, final String followed) {
        followedByUser.put(follower, new HashSet<>(Arrays.asList(followed)));
    }

}
