package net.pravdeol.twitt.command;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.pravdeol.twitt.Clock;
import net.pravdeol.twitt.Message;

public class TwittCommandFactory {
    
    private final List<Message> twittLine;
    private final Clock clock;
    private Map<String, Set<String>> followedByUser;
    
    public TwittCommandFactory(final Clock clock, final List<Message> twittLine, 
            final Map<String, Set<String>> followedByUser) {
        this.clock = clock;
        this.twittLine = twittLine;
        this.followedByUser = followedByUser;
    }

    public TwittCommand makeCommandFromInput(final String input) {
        final List<? extends TwittCommand> allPossibleCommands = createAllKnownCommands(input);
        TwittCommand command = selectCommand(allPossibleCommands);
        if (command == null) {
            command = TwittCommand.ERROR_COMMAND;
        }
        return command;
    }

    private TwittCommand selectCommand(final List<? extends TwittCommand> candidates) {
        TwittCommand command = null;
        for (TwittCommand candidate : candidates) {
            if (candidate.accept()) {
                command = candidate;
                break;
            }
        }
        return command;
    }

    private List<? extends TwittCommand> createAllKnownCommands(final String input) {
        return Arrays.asList(
                new PostMessageCommand(clock, twittLine, input),
                new ReadUserCommand(clock, twittLine, input),
                new FollowUserCommand(clock, followedByUser, input),
                new WatchWallCommand(clock, twittLine, followedByUser, input));
    }
    
}
