package net.pravdeol.twitt.command;

import java.util.Collections;
import java.util.List;

import net.pravdeol.twitt.Clock;
import net.pravdeol.twitt.Message;

public class PostMessageCommand extends AbstractTwittCommand {

    private static final String MESSAGE = ".+";
    private static final String COMMAND_REGEXP_STRING = "\\s*(" + USERNAME + ")\\s*->\\s*(" + MESSAGE + ")";
    private final List<Message> twittLine;

    public PostMessageCommand(final Clock clock, final List<Message> twittLine,
            final String input) {
        super(clock, COMMAND_REGEXP_STRING, input);
        this.twittLine = twittLine;
    }

    @Override
    public List<String> execute() {
        twittLine.add(new Message(matcher.group(1), matcher.group(2).trim(), clock.currentTime()));
        return Collections.emptyList();
    }

}
