package net.pravdeol.twitt.command;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.pravdeol.twitt.Clock;

public abstract class AbstractTwittCommand implements TwittCommand {

    protected static final String USERNAME = "[\\w]+";
    protected static final String SPACE = "\\s*";

    protected final String input;
    protected final Clock clock;
    protected final Matcher matcher;

    
    public AbstractTwittCommand(final Clock clock, final String commandStringPattern, final String input) {
        this.clock = clock;
        this.input = input;
        this.matcher = Pattern.compile(commandStringPattern).matcher(input);
    }
    
    @Override
    public boolean accept() {
        return matcher.matches();
    }
}
