package net.pravdeol.twitt.command;

import java.util.List;
import java.util.Map;
import java.util.Set;

import net.pravdeol.twitt.Clock;
import net.pravdeol.twitt.Message;

public class WatchWallCommand extends ReadUserCommand {

    private static final String COMMAND_REGEXP_STRING = "\\s*(" + USERNAME + ")\\s+wall\\s*";
    private final Map<String, Set<String>> followedByUser;

    public WatchWallCommand(final Clock clock, final List<Message> twittLine, final Map<String, Set<String>> followers,
            final String input) {
        super(clock, twittLine, COMMAND_REGEXP_STRING, input);
        this.followedByUser = followers;
    }

    @Override
    protected String format(final Message message) {
        final String timePassed = timePassed(message);
        return String.format("%s - %s (%s ago)", message.getAuthor(), message.getText(), timePassed);
    }
    
    @Override
    protected boolean isMessageToShow(final Message message) {
        return isAuthorOfMessage(message) || isFollowTheAuthor(message);
    }

    private boolean isFollowTheAuthor(final Message message) {
        final String user = matcher.group(1);
        final Set<String> followed = followedByUser.get(user);
        return followed != null && followed.contains(message.getAuthor()); 
    }

}
