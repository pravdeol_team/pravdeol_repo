package net.pravdeol.twitt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

public class IOController {

    private final BufferedReader inputReader;
    private final PrintStream printStream;
    private final Twitt twitt;
    private String inputLine;

    public IOController(final BufferedReader reader, final PrintStream out, final Clock clock) {
        this.inputReader = reader;
        this.printStream = out;
        this.twitt = new Twitt(clock);
    }

    public void readAndProcess() throws IOException {
        printPrompt();
        while (readInput(inputReader)) {
            processInput(inputLine);
            printPrompt();
        }
    }

    private boolean readInput(final BufferedReader reader) throws IOException {
        inputLine = reader.readLine();
        return inputLine != null && !inputLine.isEmpty();
    }

    private void printPrompt() {
        printStream.print(">");
    }

    private void processInput(final String input) {
        final List<String> response = twitt.process(input);
        if (response.isEmpty()) {
            return;
        } else {
            printResponse(response);
        }
    }

    private void printResponse(final List<String> response) {
        printEmptyLine();
        for (final String line : response) {
            printStream.println(line);
        }
        printEmptyLine();
    }

    private void printEmptyLine() {
        printStream.println();
    }

}
