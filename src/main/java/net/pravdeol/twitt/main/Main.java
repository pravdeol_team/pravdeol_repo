package net.pravdeol.twitt.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import net.pravdeol.twitt.Clock;
import net.pravdeol.twitt.IOController;

public class Main {

    private Main() {
    }
    
    public static void main(final String... args) throws Exception {
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            new IOController(reader, System.out, new Clock()).readAndProcess();
        }
    }

}
