package net.pravdeol.twitt;

public class TimeFormatter {

    private static final double MILLISECONDS_IN_SECOND = 1_000;
    private static final double MILLISECONDS_IN_MINUTE = 59_500;  //count on rounding, 60 sec - 0.5 sec
    private static final double MILLISECONDS_IN_HOUR = 3_570_000; //count on rounding, 60 min - 30 sec

    public String millisToReadableRoundedTime(final long timeInMillis) {
        if (lessThenMinute(timeInMillis)) {
            return readableTimeInSeconds(timeInMillis);
        } else if (lessThenHour(timeInMillis)) {
            return readableTimeInMinutes(timeInMillis);
        } else {
            return readableTimeInHours(timeInMillis);
        }
    }

    private boolean lessThenMinute(final long timeInMillis) {
        return timeInMillis < MILLISECONDS_IN_MINUTE;
    }
    
    private boolean lessThenHour(final long timeInMillis) {
        return timeInMillis < MILLISECONDS_IN_HOUR;
    }
    
    private String readableTimeInSeconds(final long timeInMillis) {
        return readableNumberWithUnits(timeInMillis / MILLISECONDS_IN_SECOND, "second");
    }
    
    private String readableTimeInMinutes(final long timeInMillis) {
        return readableNumberWithUnits(timeInMillis / MILLISECONDS_IN_MINUTE, "minute");
    }
    
    private String readableTimeInHours(final long timeInMillis) {
        return readableNumberWithUnits(timeInMillis / MILLISECONDS_IN_HOUR, "hour");
    }
    
    private String readableNumberWithUnits(final double number, final String unit) {
        final long roundedNumber = Math.round(number);
        if (roundedNumber == 0) {
            return mixNumberWithUnits(1, unit); // minimal accountable number is 1
        } else {
            return mixNumberWithUnits(roundedNumber, unit);
        }
    }

    private String mixNumberWithUnits(final long number, final String unit) {
        if (number == 1) {
            //singular
            return String.format("%s %s", number, unit);
        } else {
            //plural units suffix
            return String.format("%s %ss", number, unit);
        }
    }
}
