package net.pravdeol.twitt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.pravdeol.twitt.command.TwittCommand;
import net.pravdeol.twitt.command.TwittCommandFactory;

public class Twitt {

    private final List<Message> messages = new ArrayList<>();
    private final TwittCommandFactory commandFactory;
    private final Map<String, Set<String>> followedByUser = new HashMap<>();

    public Twitt(final Clock clock) {
        this.commandFactory = new TwittCommandFactory(clock, messages, followedByUser);
    }
    
    public List<String> process(final String input) {
        final TwittCommand command = commandFactory.makeCommandFromInput(input);
        return command.execute();
    }
}
