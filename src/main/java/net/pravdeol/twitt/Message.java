package net.pravdeol.twitt;

public class Message {
    private final String author;
    private final String text;
    private final long creationTime;
    
    public Message(final String author, final String text, final long creationTime) {
        this.author = author;
        this.text = text;
        this.creationTime = creationTime;
    }
    
    public String getAuthor() {
        return author;
    }
    
    public String getText() {
        return text;
    }
    
    public long getCreationTime() {
        return creationTime;
    }
}
